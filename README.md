# googleCalendarEmbed

## Getting Started
### Set Up Permissions & Generate iframe Link

* Navigate to the Google Calendar you wish to embed

* Click on the **Settings and sharing** link from the options menu to the right of the calendar

![Image of Settings and sharing](/img/settingsAndSharing.png)

* Scroll to the **Access permissions** section of the page

![Image of Access Permissions](/img/accessPermissions.png)

* Check the **Make available to public** box to allow the calendar to be viewable by anyone
  * This will allow the calendar and events to be viewable by anyone, but it will still only be editable by the calendar owner

* Scroll down to the **Integrate calendar** section of the page

![Image of Access Permissions](/img/integrateCalendar.png)

* Copy the code from the **Embed code** field

### Add the iframe to the HTML File

* Open the **googleCalendarIFrame.html** file

* Replace the iframe in the file with the one that you copied from earlier
  * Be sure to add in `id="calendarIFrame"` like in the example iframe.

## Embed the Page

* The googleCalendarIFramge.html page made in the previous section can now be used in a website like any standard
html page

* The result should be a fullscreen calendar that displays events from your Google Calendar
